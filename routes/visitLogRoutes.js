import express from "express"
import { createVisitLog, getVisitLogs } from "../controllers/visitLogController.js"
import { verify } from "../middlewares/verify.js"

const router = express.Router()

router.get("/", verify, getVisitLogs)
router.post("/", verify, createVisitLog)

export default router