import { prisma } from "../prisma/prismaClient.js";

export const createCategory = async (req, res) => {
  const { category_name } = req.body;
  const { user_id } = req.user

  console.log(req.user)

  try {
    const user = await prisma.user.findUnique({
      where: { id: user_id },
    });

    if (!user) {
      return res.status(404).json({ error: "User not found" });
    }

    const newCategory = await prisma.category.create({
      data: {
        user_id,
        category_name,
      },
    });

    res.status(201).json(newCategory);
  } catch (error) {
    console.error("Error creating category:", error);
    res.status(500).json({ error: "Failed to create category" });
  }
};

export const getAllCategoriesByUser = async (req, res) => {
  const { user_id } = req.user;

  try {
    const categories = await prisma.category.findMany({
      where: { user_id: parseInt(user_id, 10) },
      include: { user: true },
    });

    res.status(200).json(categories);
  } catch (error) {
    console.error("Error fetching categories:", error);
    res.status(500).json({ error: "Failed to fetch categories" });
  }
};

export const updateCategory = async (req, res) => {
  const { categoryId } = req.params;
  const { category_name } = req.body;

  try {
    const updatedCategory = await prisma.category.update({
      where: { id: parseInt(categoryId, 10) },
      data: { category_name },
    });

    res.status(200).json(updatedCategory);
  } catch (error) {
    console.error("Error updating category:", error);
    res.status(500).json({ error: "Failed to update category" });
  }
};

export const deleteCategory = async (req, res) => {
  const { categoryId } = req.params;

  try {
    await prisma.category.delete({
      where: { id: parseInt(categoryId, 10) },
    });

    res.status(204).end();
  } catch (error) {
    console.error("Error deleting category:", error);
    res.status(500).json({ error: "Failed to delete category" });
  }
};
