import express from "express";
import {
  createCategory,
  getAllCategoriesByUser,
  updateCategory,
  deleteCategory
} from "../controllers/category.js";
import { verify } from "../middlewares/verify.js";

const router = express.Router();

router.get("/:userId", verify, getAllCategoriesByUser);
router.put("/:categoryId", verify, updateCategory);
router.post("/new", verify, createCategory);
router.delete("/:categoryId", verify, deleteCategory)

export default router;
