import express from "express";
import dotenv from "dotenv";
import bodyParser from "body-parser";
import cors from "cors"
import { prisma } from "./prisma/prismaClient.js";

import categoryRoutes from "./routes/categoryRoutes.js";
import userRoutes from "./routes/userRoutes.js"
import visitLogRoutes from "./routes/visitLogRoutes.js"

dotenv.config();
const app = express();
app.use(cors())
app.use(bodyParser.json());

async function checkDatabaseConnection() {
  try {
    await prisma.$connect();
    console.log("Database connected successfully 💪🏻");
  } catch (error) {
    console.error("Database connection error:", error);
    process.exit(1);
  }
}

app.use("/category", categoryRoutes);
app.use("/user", userRoutes);
app.use("/visitlog", visitLogRoutes)

app.listen(process.env.PORT, async () => {
  console.log(`The Server Started 🚀 at http://localhost:${process.env.PORT}`);
  await checkDatabaseConnection();
});
