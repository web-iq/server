import { prisma } from "../prisma/prismaClient.js"

export const getVisitLogs = async (req, res) => {
  try {
    // Retrieve the user ID from req.user
    const userId = req.user.user_id;

    // Fetch visit logs associated with the user ID
    const visitLogs = await prisma.visitLog.findMany({
      where: {
        user_id: userId,
      },
      include: {
        website: true,
      },
    });

    // If there are no visit logs, respond with a 404 status code
    if (visitLogs.length === 0) {
      return res.status(404).json({ message: "No visit logs found for this user." });
    }

    // Respond with the retrieved visit logs
    res.status(200).json(visitLogs);
  } catch (error) {
    console.error("Error retrieving visit logs:", error);
    res.status(500).json({ error: "Internal server error" });
  }
};

export const createVisitLog = async (req, res) => {
  try {
    const { domain, visited_at, duration } = req.body;
    const { user_id } = req.user;

    const formattedVisitedAt = new Date(visited_at).toISOString();

    const website = await findOrCreateWebsite(domain, user_id);

    const log = await prisma.visitLog.create({
      data: {
        user: { connect: { id: user_id } },
        website: { connect: { id: website.id } },
        visited_at: formattedVisitedAt,
        duration: duration,
      },
    });

    res.status(201).json(log);
  } catch (error) {
    console.error("Error creating visit log:", error);
    res.status(500).json({ error: "Internal server error" });
  } finally {
    await prisma.$disconnect();
  }
};

async function findOrCreateWebsite(domain, userId) {
  try {
    const existingWebsite = await prisma.website.findUnique({
      where: {
        domain: domain,
      },
    });

    const miscCategory = await prisma.category.findUnique({
      where: {
        category_name: "Misc",
        user_id: userId,
      },
    });

    if (!existingWebsite) {
      const website = await prisma.website.create({
        data: {
          user: { connect: { id: userId } },
          category: { connect: { id: miscCategory.id } },
          domain: domain,
        },
      });

      return website;
    } else {
      return existingWebsite;
    }
  } catch (error) {
    console.error("Error finding or creating website:", error);
    throw new Error("Unable to find or create website");
  }
}
