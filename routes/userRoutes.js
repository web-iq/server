import express from "express";
import {
  registerUser,
  authenticateUser,
  getUserById,
  updateUser
} from "../controllers/userController.js"
import { verify } from "../middlewares/verify.js";

const router = express.Router();

// Routes for user management
router.post('/register', registerUser);
router.post('/login', authenticateUser);
router.get('/:userId', verify, getUserById);
router.put('/:userId', verify, updateUser);

export default router