# WebIQ Server

This is an Express / NodeJS Web server. Here are the steps to start the server.

We are using yarn as our JavaScript Package Manager. You can download the dependencies using this command.
```
yarn install
```

Before starting the server you need to have a .env file in the project directory. Create a .env file, and add this environment variable

```
PORT=3535
```

You can start the development server using this command
```
yarn dev
```